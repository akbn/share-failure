import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import RNFS from 'react-native-fs';
import Share from 'react-native-share';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  state = {}

  componentDidMount() {
    const filepath = `${RNFS.DocumentDirectoryPath}/sample.jpeg`;
    RNFS.downloadFile({
                    fromUrl: 'https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg',
                    toFile: filepath,
                  }).promise.then(({ statusCode, bytesWritten }) => {
                    console.log('Download complete:', statusCode, bytesWritten, filepath)
                    this.setState({filepath})
                  })
  }

  _share = async () => {
      const {filepath} = this.state;
      const { dismissedAction, app } = await Share.open({
      url: `file://${filepath}`,
      title: 'Share test title',
      subject: 'Share test message',
      message: 'Share test message',
      failOnCancel: false,
    });
    console.log('dismissedAction', dismissedAction)
    console.log('app', app)
  }

  render() {
    const {filepath} = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to Share Failure!</Text>
        {filepath && <View>
          <Text style={styles.instructions}>Your file is ready to share!</Text>
          <TouchableOpacity style={{backgroundColor: 'yellow', padding: 5, margin: 10, justifyContent: 'center', alignItems: 'center'}} onPress={this._share}><Text>Share</Text></TouchableOpacity>
        </View>}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
